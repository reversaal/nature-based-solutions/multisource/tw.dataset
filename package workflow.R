# Sophie Guillaume
# Package workflow for data validation of treatment wetland datasets
library(devtools)

### Initialize ################################################################
# use_git()
# use_mit_license()
# writeLines(readLines("LICENSE"))

### Update ####################################################################
devtools::check()
devtools::document() # converts special comment of function into man/function.Rd and updates NAMESPACE file based on @export tags in roxygen comments
devtools::build()
devtools::install()
devtools::load_all()

#use_readme_rmd()
build_readme()

#rename_files("indexna", "Index_NA")

### packages I will need ######################################################
# use_package("stats")
# use_package("na.exclude")
# use_package("optimize")
# use_package("pexp")
# use_package("plnorm")
# use_package("pnorm")

# use_package("Metrics")
# use_package("dplyr")
# use_package("mlr3measures")
# use_package("psy")
# use_package("rlang")
# use_package("scales")
# use_package("stats")
# use_package("corrplot")
# use_package("units")
# use_package("na.omit")
# use_package("stringr")
# use_package("truncnorm")
# use_package("FactoMineR")
# use_package("missMDA")
# use_package("caret")
# use_package("ggplot2")
# use_package("matrixStats")
# use_package("data.table")
# use_package("gridExtra")
# use_package("units")
# use_package("MASS")

### functions I will store ####################################################
# Use capital letter for the first letter
# Use underscore to separate different words
# Use capital letters for acronyms

# Exploratory multivariate analysis

use_r("PCA_not_imputed") # Runs PCA without imputation of data
use_r("PCA_imputed") # Runs PCA with imputation of data
# need to add FAMD of MCA for categorical variables !
use_r("FPCA_expr") # Runs Focused PCA

# manage NA and 'outliers'

use_r("Col_enough_data") # Removes columns with too many NA from dataframe
use_r("Index_NA") # Removes lines with too many NA from dataframe
use_r("Remove_outliers") # Removes outliers according to boxplot IQR interval

# for dataset construction

use_r("Change_equivalent") # Transforms names and concentrations of compounds into their N or P equivalents
use_r("GPS_conversion") # Converts GPS coordinates from degrees/minutes/seconds to decimal degrees
use_r("Check_relations") # Checks relations between compounds
use_r("Check_ratio") # Checks ratio between compounds for raw wastewater
use_r("Check_concentrations") # Checks min and max concentration intervals
use_r("Check_Chauvenet") # Check how probable it is to obtain the observation, assuming a known distribution.
use_r("Check_Z_score") # Checks how many sd away from the mean the observation is.
use_r("Check_Chauvenet_HLR") # Checks how probable it is to obtain the HLR, assuming a known distribution.
use_r("Check_performance_removal") # Checks the performance removal of the Treatment Wetland
use_r("New_variables") # Creates new variables by combination of initial variables

use_r("Get_flag") # Gives a number to the flags (scoring from category)
use_r("Get_flag_worst") # Concatenates checks into one flag
use_r("Get_score_worst") # Concatenates checks into one score (scoring)
use_r("Get_flag_worst_for_model") # Concatenates checks into one flag (scoring)
use_r("Get_cat_score") # Transforms flag into a score




# Check where the function is stored, it should not appear in the environment (invisible, return F)
exists("Linear_reg", where = globalenv(), inherits = FALSE)
?linear_reg

### checks of functionality ###################################################
# check outer functions to check the inner functions
use_testthat()
use_test("linear_reg")

### add and modify a dataset ##################################################

usethis::use_data_raw("dict_location.json")
usethis::use_data_raw("substrate_imputed.json")

usethis::use_data(HSSF)
usethis::use_data(VSSF)
usethis::use_data_raw("HSSF")
usethis::use_data_raw("VSSF")
invisible(HSSF)
invisible(VSSF)

# to store together multiple data objects, these won't be exported so no need to document them. No impact on description file.

#internal_climate <- here("data-raw","dict_location.json")
#internal_substrate <- here("data-raw","substrate_imputed.json")

#usethis::use_data(internal_climate, internal_substrate, internal = TRUE)










