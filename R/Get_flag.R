
#' Get flag - numerical score
#'
#' @param df A dataframe with the checks and flags
#'
#' @return The dataframe with numerical scores rather than labels
#' @export
#'
#' @examples
Get_flag <- function(df){

  df_score <- df |>
    # unchecked because lack of expert knowledge o
    mutate(quality_water_type = stringr::str_replace_all(quality_water_type, 'unchecked', "0")) |> # unchecked should not be considered in concatenation?
    mutate(quality_concentration = str_replace_all(quality_concentration, 'unchecked', "0")) |>
    mutate(compliance_Chauvenet = str_replace_all(compliance_Chauvenet, 'unchecked', "0")) |>
    mutate(compliance_Chauvenet_out = str_replace_all(compliance_Chauvenet_out, 'unchecked', "0")) |>
    mutate(quality_measure_in = str_replace_all(quality_measure_in, 'unchecked', "0")) |>
    mutate(quality_measure_out = str_replace_all(quality_measure_out, 'unchecked', "0")) |>

    mutate(compliance_Chauvenet_HLR_surface = str_replace_all(compliance_Chauvenet_HLR_surface, 'unchecked', "0")) |>
    mutate(compliance_Chauvenet_HLR_CS = str_replace_all(compliance_Chauvenet_HLR_CS, 'unchecked', "0")) |>
    mutate(flag = str_replace_all(flag, 'unchecked', "0")) |>
    # unsuitable
    #mutate(goodness_performance = str_replace_all(goodness_performance, 'unlikely', "1")) |>
    mutate(quality_water_type = str_replace_all(quality_water_type, 'unlikely', "1")) |>
    mutate(quality_concentration = str_replace_all(quality_concentration, 'unlikely', "1")) |>
    mutate(compliance_Chauvenet = str_replace_all(compliance_Chauvenet, 'unlikely', "1")) |>
    mutate(compliance_Chauvenet_out = str_replace_all(compliance_Chauvenet_out, 'unlikely', "1")) |>
    mutate(quality_measure_in = str_replace_all(quality_measure_in, 'unlikely', "1")) |>
    mutate(quality_measure_out = str_replace_all(quality_measure_out, 'unlikely', "1")) |>

    mutate(compliance_Chauvenet_HLR_surface = str_replace_all(compliance_Chauvenet_HLR_surface, 'unlikely', "1")) |>
    mutate(compliance_Chauvenet_HLR_CS = str_replace_all(compliance_Chauvenet_HLR_CS, 'unlikely', "1")) |>
    mutate(flag = str_replace_all(flag, 'unsuitable', "1")) |>
    # doubftul
    #mutate(goodness_performance = str_replace_all(goodness_performance, 'unusual', "0.5")) |>
    mutate(quality_water_type = str_replace_all(quality_water_type, 'unusual', "2")) |>
    mutate(quality_concentration = str_replace_all(quality_concentration, 'unusual', "2")) |>
    mutate(compliance_Chauvenet = str_replace_all(compliance_Chauvenet, 'unusual', "2")) |>
    mutate(compliance_Chauvenet_out = str_replace_all(compliance_Chauvenet_out, 'unusual', "2")) |>
    mutate(quality_measure_in = str_replace_all(quality_measure_in, 'unusual', "2")) |>
    mutate(quality_measure_out = str_replace_all(quality_measure_out, 'unusual', "2")) |>

    mutate(compliance_Chauvenet_HLR_surface = str_replace_all(compliance_Chauvenet_HLR_surface, 'unusual', "2")) |>
    mutate(compliance_Chauvenet_HLR_CS = str_replace_all(compliance_Chauvenet_HLR_CS, 'unusual', "2")) |>
    mutate(flag = str_replace_all(flag, 'doubtful', "2")) |>
    # good
    # mutate(goodness_performance = str_replace_all(goodness_performance, 'likely', "0")) |>
    mutate(quality_water_type = str_replace_all(quality_water_type, 'likely', "3")) |>
    mutate(quality_concentration = str_replace_all(quality_concentration, 'likely', "3")) |>
    mutate(compliance_Chauvenet = str_replace_all(compliance_Chauvenet, 'likely', "3")) |>
    mutate(compliance_Chauvenet_out = str_replace_all(compliance_Chauvenet_out, 'likely', "3")) |>
    mutate(quality_measure_in = str_replace_all(quality_measure_in, 'likely', "3")) |>
    mutate(quality_measure_out = str_replace_all(quality_measure_out, 'likely', "3")) |>

    mutate(compliance_Chauvenet_HLR_surface = str_replace_all(compliance_Chauvenet_HLR_surface, 'likely', "3")) |>
    mutate(compliance_Chauvenet_HLR_CS = str_replace_all(compliance_Chauvenet_HLR_CS, 'likely', "3")) |>
    mutate(flag = str_replace_all(flag, 'good', "3"))

  return(df_score)

}
