#' Scoring concatenation of multiple checks
#'
#' @param df Dataframe containing a column with the result for each check
#'
#' @return The same dataframe with an additional column for the flag
#' @export
#'
#' @examples
Get_score_worst <- function(df){
  df$score <- NA
  unique_concentration <- unique(df$id_concentrations)

  for(concentration in unique_concentration){
    # NA not considered for flags

    df$score[df$id_concentrations == concentration] <- min(df$score_Chauvenet_in[df$id_concentrations == concentration],
                                                          df$score_Chauvenet_out[df$id_concentrations == concentration],
                                                          df$score_water_type[df$id_concentrations == concentration],
                                                          df$score_measure_in[df$id_concentrations == concentration],
                                                          df$score_measure_out[df$id_concentrations == concentration],
                                                          na.rm=T)
  }

  # If data is not processed through the checks
  index_exists <- !is.na(df$compliance_Chauvenet)
  df$score[index_exists == F] <- NA

  return(df)

}
