
<!-- README.md is generated from README.Rmd. Please edit that file -->

**NOTE: this package is built in the context of my PhD, to handle
limited knowledge and data for modeling of treatment wetlands**

# tw.dataset package

<!--
Title: Name given to the resource: the title is usually the formal name by which the resource is
known.
Creator: The entity primarily responsible for creating the content of the resource.
Subject: The topic of the resource content. Generally, the topic is expressed in the form of
keywords or phrases
Description: A presentation of the content of the resource. Examples of descriptions include:
abstract, table of contents, reference to a graphical representation of the content, free-text
presentation of the content.
Publisher: The entity responsible for making the resource available or disseminating it.
Contributor: An entity responsible for contributions to the content of the resource. Examples of
contributors include an individual, organization, or department.
Date: Date of an event in the life cycle of the resource. This will usually be the date the resource
was created or made available.
Type: Nature of the resource. At least one Type element conforms to the DCMI Type Vocabulary
which includes 12 values: collection, dataset, event, image, interactive resource, moving image,
physical
object,
service,
software,
sound,
still
image,
text.
(http://dublincore.org/documents/dcmi-type-vocabulary/)
Format: The physical or digital manifestation of the resource. The values for this element, for the
description of a digital resource, come from the list of MIME (Multipurpose Internet Mail
Extensions) types maintained by The Internet Corporation for Assigned Names and Numbers.
(http://www.iana.org/assignments/media-types/)
Identifier: Unambiguous reference to the resource in a given context. (DOI)
Source: Reference to a resource from which the described resource is derived.
Language: The language of the resource
Relation: Reference to a related resource.
Coverage: Perimeter or scope of the resource's content, i.e., the spatial and temporal coverage
of the resource. It is recommended that a value be selected from a controlled vocabulary (e.g.,
the TGN Thesaurus of Geographic Names) and that, where appropriate, place or period names
be used rather than numeric identifiers such as coordinates or date ranges.
(http://www.getty.edu/research/conducting_research/vocabularies/tgn/)
Rights: rights associated with the resource
-->
<!-- badges: start -->
<!-- badges: end -->

The goal of tw.dataset is to provide TW professionals with modelling
tools (functions) to help them handle and use wisely a dataset with
limited size, combined with knowledge. The package covers the modeling
of “pollutant” removal from an inflow to an outflow concentration (or
load). It is therefore important to follow a rigorous methodology, in
terms of data validation, selection of explanatory variables, training
of models and tuning, and model validation and comparison.

## Installation

You can install the development version of tw.dataset like so:

``` r
library(tw.dataset)
```

## Dataset provided

The dataset provided is the result of a compilation of scientific papers
gathered from a literature review. The raw data is is stored in a
database hosted by ICRA, Girona. The dataset gathers design and
operational characteristics of Horizontal Flow Treatment Wetlands,
hydraulics and pollutant removals of usual pollutants (BOD, NH4, TN,
PO4, TP, TSS). The context (size, type of wastewater, location) of the
studies is also provided along with reference to the papers.

Three individual datasets, on HFTW, VFTW and FWS. They can be merged to
obtain treatment chains.

### Data validation

Change() function provided in the package allow conversion of pollutants
names and concentrations to their N and P-equivalents. Functions to
check data are provided: Check_concentration() for ranges of
concentrations, Check_ratio() for pairwise ratios between pollutants
(only available for raw or primary treated wastewater) and
Check_relations() for relationships between pollutants concentrations.

The database also includes Vertical Flow and Free Surface Flow Treatment
Wetlands. Additional data retrieved from other sources than the papers
are the following: \* climate: climates over period 1901-2010 data from:
Chen et Chen 2013, <https://doi.org/10.1016/j.envdev.2013.03.007>,
<http://hanschen.org/koppen> provided in .tsv format \* average
temperature ($^{\circ}C*10$), average precipitation ($mm/yr$): average
over period 1981-2010 with 30 arc seconds data from: Karger et al 2017,
<https://doi.org/10.1038/sdata.2017.122>,
<https://chelsa-climate.org/downloads/> provided in .tif format \*
elevation ($m$): 15 arc seconds data from: Lehner et al 2008,
<https://doi.org/10.1029/2008EO100001>,
<https://www.hydrosheds.org/products/hydrosheds> provided in .tif format

This additional data (from geolocation and raster formats) was extracted
using R packages depending on *gdal* package that is no longer updated.
